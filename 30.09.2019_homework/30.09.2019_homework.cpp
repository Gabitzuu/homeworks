#include "pch.h"	
#include <iostream>
#include <fstream>
using namespace std;

void problema1();
void problema2();
int problema3();
void problema4();
void problema4_2();

ifstream fin("date.in");
ofstream fout("date.out");

int main() {
	problema4_2();
}

void problema1() {
	int x, z = 1, y;

	fin >> x;

	do {
		fin >> y;
		for (int d = 2; d < y; d++) {
			if (x%d == 0 && y%d == 0 && d > z) {
				z = d;
			}
		}
		x = y;
	} while (y = 0);
	fout << z;
}
void problema2() {
	int n, k, p = 0, i = 1, x;

	fin >> n >> k;

	while (i <= n) {
		x = i;
		while (x%k == 0) {
			x = abs(x / k);
			p = p + 1;
		}
		i = i + 1;
	}


	fout << p;
}

int problema3() {
	struct tdata {
		int zi, luna;
	} d;
	struct eveniment {
		int nr;
		struct tdata dev;
	} e;

	d.zi = 19;
	d.luna = 9;
	e.nr = 1;
	e.dev.zi = 19;
	e.dev.luna = 9;

	if (d.luna <= e.dev.luna) {
		if (d.zi < e.dev.zi) {
			return 1;
		}
		else return 0;
	}
	return 0;
}

void problema4() {
	int n, m = 0, x = 1, cp;

	fin >> n;

	while (x <= 9) {
		cp = n;
		while (cp != 0) {
			if (cp % 10 == x) {
				m = m * 10 + x;
			}
			cp = abs(cp / 10);
		}
		x = x + 1;
	}
	fout << m;
}

// 11, 101, 1001
void problema4_2() {
	int n, m = 0, x = 1, cp;
	fin >> n;

	while (x <= 9) {
		cp = n;
		while (cp != 0) {
			if (cp % 10 == x) {
				m = m * 10 + x;
			}
			cp = abs(cp / 10);
		}
		x = x + 1;
	}
	fout << m;
}